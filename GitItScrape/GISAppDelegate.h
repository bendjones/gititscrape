//
//  GISAppDelegate.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class GISOverviewWindowController;
@interface GISAppDelegate : NSObject <NSApplicationDelegate>
@property (strong, nonatomic) GISOverviewWindowController *overviewWindowController;
@end
