//
//  GISOverviewWindowController.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "GISEntryObjectDelegate.h"
#import "GISScrapeControllerDelegate.h"

@class GISScrapeController;
@interface GISOverviewWindowController : NSWindowController <GISScrapeControllerDelegate, GISEntryObjectDelegate>
@property (strong, nonatomic) GISScrapeController *scrapeController;
@end
