//
//  GISOverviewWindowController.m
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import "GISOverviewWindowController.h"
#import "GISScrapeController.h"

#import "GISEntryObject.h"

@interface GISOverviewWindowController ()
@property (strong, nonatomic) IBOutlet NSLevelIndicator *jobLevelIndicator;
@property (strong, nonatomic) IBOutlet NSTextView *logTextView;
@property (strong, nonatomic) NSString *logOutput;
@property (strong, nonatomic) NSTimer *logOutputTimer;
@property (strong, nonatomic) IBOutlet NSArrayController *nodeArrayController;
@end

@implementation GISOverviewWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        self.scrapeController = [[GISScrapeController alloc] initWithDelegate:self];
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    [self.jobLevelIndicator setIntegerValue:0];
    [self.logTextView setString:@"FOO!"];
    
    [self.scrapeController startWork];
    
    self.logOutputTimer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(updateLogOutput) userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.logOutputTimer forMode:NSRunLoopCommonModes];
}

- (NSString *)windowNibName
{
	return @"GISOverviewWindowController";
}

- (IBAction)rerun:(id)sender {
    [self.scrapeController startWork];
}

- (void)updateLogOutput {
    [self.logTextView setString:self.logOutput];
}

#pragma mark GISEntryObjectDelegate
- (void)entryAddedToFirebase:(GISEntryObject *)entryObject {
    [self performSelectorOnMainThread:@selector(addNode:) withObject:entryObject waitUntilDone:YES];
}

#pragma mark GISScrapeControllerDelegate Methods
- (void)processedNodeCreated:(GISEntryObject *)newEntryObject {
    [newEntryObject uploadToFirebaseWithDelegate:self];
}

- (void)addNode:(GISEntryObject *)entryObject {
    [self.nodeArrayController addObject:entryObject];
}

- (void)logResult:(NSString *)logEntry {
    NSString *currentEntry = [NSString stringWithFormat:@"%@", logEntry];
    self.logOutput = currentEntry;
    NSLog(@"%@", self.logOutput);
    
    [self updateLogOutput]; 
}

- (void)downloadsStarted {
    
}

- (void)downloadsFinished {
    
}

- (void)downloadStatusChanged:(NSArray *)statusDescription {
    [self.jobLevelIndicator setMaxValue:[statusDescription[1] intValue]];
    [self.jobLevelIndicator setIntValue:[statusDescription[0] intValue]];
}

@end
