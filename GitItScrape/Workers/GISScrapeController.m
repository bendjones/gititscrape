//
//  GISScrapeController.m
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

// http://sfgate.kaango.com/browse/garage-tag-estate-yard-sales/25/

#import "GISScrapeController.h"
#import "GISScrapeDownloader.h"
#import "GISDescriptionEntryParser.h"
#import "GISDescriptionLinkParser.h"
#import "GISEntryObject.h"
#import "HTMLParser.h"

@interface GISScrapeController ()
@property (strong, nonatomic) NSMutableString *textInProgress;
@property (assign, nonatomic) NSUInteger total;
@property (assign, nonatomic) NSUInteger done;
@end

@implementation GISScrapeController
@synthesize downloaders;
@synthesize parsers;

- (id)initWithDelegate:(id<GISScrapeControllerDelegate>)delegate {
    if (self = [super init]){
        self.delegate = delegate;
        self.downloaders = [NSMutableArray array];
        self.parsers = [NSMutableArray array];
    }
    
    return self;
}

- (void)startWork {
    GISScrapeDownloader *newDownloader = [[GISScrapeDownloader alloc] init];
    
    NSURL *url = [NSURL URLWithString:@"http://sfgate.kaango.com/browse/garage-tag-estate-yard-sales/yard-garage-sales/1127/?where=San+Francisco%2C+California&distance=300&latlong=37.773792%2C-122.409865&market=2&v=2&pp=100"];
    [newDownloader downloadPageAtURL:url onSuccess:^(GISScrapeDownloader *scrapeDownloader) {
        NSString *html = [NSString stringWithUTF8String:[scrapeDownloader.returnedValue bytes]];
        NSError *htmlError;
        HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&htmlError];
        HTMLNode *bodyNode = [parser body];
        
        for (HTMLNode *node in [bodyNode findChildTags:@"div"]){
            if ([[node getAttributeNamed:@"class"] isEqualToString:@"kng-ad-results"]){
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                GISDescriptionLinkParser *resultParser = [[GISDescriptionLinkParser alloc] initWithResult:node runningInQueue:queue];
                [parsers addObject:resultParser];
                [self startNewParsers];
            }
            
            if ([[node getAttributeNamed:@"class"] isEqualToString:@"kng-pagination"]){
                for (HTMLNode *linode in [node findChildTags:@"li"]){
                    HTMLNode *aTag = [linode findChildTag:@"a"];
                    if (aTag != nil){
                        NSString *linkString = [aTag getAttributeNamed:@"href"];
                        NSLog(@"Page Link: %@", linkString);
                        [self processNextPage:linkString];
                    }
                }
            }
        }
        
    } onFailure:^(GISScrapeDownloader *scrapeDownloader) {
        NSLog(@"Failure %@", scrapeDownloader.returnedError);
    } onAny:^(GISScrapeDownloader *scrapeDownloader) {
    }];
}

- (void)processNextPage:(NSString *)nextPageLink {
    GISScrapeDownloader *newDownloader = [[GISScrapeDownloader alloc] init];
    NSURL *url = [NSURL URLWithString:nextPageLink];
    [newDownloader downloadPageAtURL:url onSuccess:^(GISScrapeDownloader *scrapeDownloader) {
        NSString *html = [NSString stringWithUTF8String:[scrapeDownloader.returnedValue bytes]];
        NSError *htmlError;
        HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&htmlError];
        HTMLNode *bodyNode = [parser body];
        
        for (HTMLNode *node in [bodyNode findChildTags:@"div"]){
            if ([[node getAttributeNamed:@"class"] isEqualToString:@"kng-ad-results"]){
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                GISDescriptionLinkParser *resultParser = [[GISDescriptionLinkParser alloc] initWithResult:node runningInQueue:queue];
                [parsers addObject:resultParser];
                [self startNewParsers];
            }
            
            if ([[node getAttributeNamed:@"class"] isEqualToString:@"kng-pagination"]){
                for (HTMLNode *linode in [node findChildTags:@"li"]){
                    HTMLNode *aTag = [linode findChildTag:@"a"];
                    if (aTag != nil){
                        NSString *linkString = [aTag getAttributeNamed:@"href"];
                        NSLog(@"Page Link: %@", linkString);
                        [self processNextPage:linkString];
                    }
                }
            }
        }
        
    } onFailure:^(GISScrapeDownloader *scrapeDownloader) {
        NSLog(@"Failure %@", scrapeDownloader.returnedError);
    } onAny:^(GISScrapeDownloader *scrapeDownloader) {
        
    }];
}

- (void)processEntryDescriptions:(NSString *)descriptionPageLink {
    GISScrapeDownloader *newDownloader = [[GISScrapeDownloader alloc] init];
    self.total += 1;
    [newDownloader downloadPageAtURL:[NSURL URLWithString:descriptionPageLink] onSuccess:^(GISScrapeDownloader *scrapeDownloader) {
        NSString *html = [NSString stringWithUTF8String:[scrapeDownloader.returnedValue bytes]];
        NSError *htmlError;
        HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&htmlError];
        HTMLNode *bodyNode = [parser body];
        GISDescriptionEntryParser *resultParser = [[GISDescriptionEntryParser alloc] initWithEntry:bodyNode];
        [resultParser parseNode:^(GISEntryObject *createdEntryObject) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(logResult:)]){
                [self.delegate logResult:[createdEntryObject description]];
                if ([self.delegate respondsToSelector:@selector(processedNodeCreated:)])
                    [self.delegate processedNodeCreated:createdEntryObject];
            }
        }];
    } onFailure:^(GISScrapeDownloader *scrapeDownloader) {
        NSLog(@"Failure %@", scrapeDownloader.returnedError);
    } onAny:^(GISScrapeDownloader *scrapeDownloader) {
        [self updateProgress:YES];
    }];
}

- (void)updateProgress:(BOOL)itemFinished {
    if (itemFinished)
        self.done += 1;
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(downloadStatusChanged:)]){
        [self.delegate downloadStatusChanged:@[@(self.done), @(self.total)]];
    }
}

- (void)startNewParsers {
    [self.parsers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![(GISDescriptionLinkParser *)obj isFinished]){
            [(GISDescriptionLinkParser *)obj processPagesForDescriptionLinks:^(NSString *foundDescriptionLink, GISDescriptionLinkParser *parser) {
                parser.isFinished = YES;
                [self processEntryDescriptions:foundDescriptionLink];
            }];
        }
    }];
}


@end
