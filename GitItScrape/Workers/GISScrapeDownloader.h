//
//  GISScrapeDownloader.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GISScrapeDownloader;
typedef void (^scapeDownloadCallback)(GISScrapeDownloader *scrapeDownloader);

@interface GISScrapeDownloader : NSObject

@property (strong, nonatomic) id returnedValue;
@property (strong, nonatomic) NSError *returnedError;

- (void)downloadPageAtURL:(NSURL *)url onSuccess:(scapeDownloadCallback)successCallback onFailure:(scapeDownloadCallback)failureCallback onAny:(scapeDownloadCallback)anyCallback;
- (void)cancel;


@end
