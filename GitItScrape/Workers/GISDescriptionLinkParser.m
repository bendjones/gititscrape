//
//  GISDescriptionLinkParser.m
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import "GISDescriptionLinkParser.h"
#import <Firebase/Firebase.h>

@interface GISDescriptionLinkParser ()
@property (strong, nonatomic) NSArray *entries;
@end

@implementation GISDescriptionLinkParser

- (id)initWithResult:(HTMLNode *)result runningInQueue:(NSOperationQueue *)queue {
    if ([super init]){
        self.queue = queue;
        self.resultNode = result;
        self.isFinished = NO;
    }
    return self;
}

- (void)processPagesForDescriptionLinks:(parserDoneCallback)callback {
    if (self.resultNode != nil){
        self.entries = [self.resultNode findChildTags:@"li"];
        [self.entries enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([[obj getAttributeNamed:@"class"] isEqualToString:@"kng-ad-results-description"]){
                HTMLNode *aLinkNode = [obj findChildTag:@"a"];
                if (aLinkNode){
                    NSString *linkString = [aLinkNode getAttributeNamed:@"href"];
                    callback(linkString, self);
                }
            }
        }];
    }
}

@end
