//
//  GISScrapeController.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GISScrapeControllerDelegate.h"

@interface GISScrapeController : NSObject
@property (weak, nonatomic) id<GISScrapeControllerDelegate>delegate;

@property (strong, nonatomic) NSMutableArray *downloaders;
@property (strong, nonatomic) NSMutableArray *parsers;

- (id)initWithDelegate:(id<GISScrapeControllerDelegate>)delegate;
- (void)startWork;

@end
