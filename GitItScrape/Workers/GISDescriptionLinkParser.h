//
//  GISDescriptionLinkParser.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HTMLNode.h"

@class GISDescriptionLinkParser;
typedef void (^parserDoneCallback)(NSString *foundDescriptionLink, GISDescriptionLinkParser *parser);

@interface GISDescriptionLinkParser : NSObject
@property (weak, nonatomic) NSOperationQueue *queue;
@property (assign, nonatomic) BOOL isFinished;
@property (strong, nonatomic) HTMLNode *resultNode;

- (id)initWithResult:(HTMLNode *)result runningInQueue:(NSOperationQueue *)queue;
- (void)processPagesForDescriptionLinks:(parserDoneCallback)callback;
@end
