//
//  GISScrapeDownloader.m
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import "GISScrapeDownloader.h"

#define kGISScrapeDownloaderRequestTimeout 30

@interface GISScrapeDownloader()
@property (strong, nonatomic) NSOperationQueue *scrapeDownloaderQueue;
@end

@implementation GISScrapeDownloader
@synthesize returnedValue;
@synthesize returnedError;

- (id)init {
    if (self = [super init]){
        self.scrapeDownloaderQueue = [[NSOperationQueue alloc] init];
    }
    
    return self;
}

- (void)downloadPageAtURL:(NSURL *)url onSuccess:(scapeDownloadCallback)successCallback onFailure:(scapeDownloadCallback)failureCallback onAny:(scapeDownloadCallback)anyCallback {
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:kGISScrapeDownloaderRequestTimeout];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.scrapeDownloaderQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error){
            self.returnedValue = data;
            successCallback(self);
        } else {
            self.returnedError = error;
            failureCallback(self);
        }
        anyCallback(self);
    }];
}

- (void)cancel {
    [self.scrapeDownloaderQueue cancelAllOperations];
}

- (void)dealloc {
    [self.scrapeDownloaderQueue cancelAllOperations];
    self.scrapeDownloaderQueue = nil;
    self.returnedError = nil;
    self.returnedError = nil;
}

@end
