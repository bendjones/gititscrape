//
//  GISDescriptionEntryParser.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTMLNode.h"

@class GISEntryObject;
typedef void (^descriptionEntryParserDone)(GISEntryObject *createdEntryObject);

@interface GISDescriptionEntryParser : NSObject
@property (weak, nonatomic) HTMLNode *node;

- (id)initWithEntry:(HTMLNode *)node;
- (void)parseNode:(descriptionEntryParserDone)callback;

@end
