//
//  GISDescriptionEntryParser.m
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import "GISDescriptionEntryParser.h"
#import "GISEntryObject.h"

@interface GISDescriptionEntryParser()
@property (strong, nonatomic) GISEntryObject *entryObject;
@end

@implementation GISDescriptionEntryParser

- (id)initWithEntry:(HTMLNode *)node {
    if (self = [super init]){
        self.node = node;
    }
    return self;
}

- (void)parseNode:(descriptionEntryParserDone)callback {
    
    if (self.node != nil){
        NSArray *entries = [self.node findChildTags:@"ul"];
        self.entryObject = [[GISEntryObject alloc] init];
        [entries enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([[obj getAttributeNamed:@"class"] isEqualToString:@"kng-two-column"]){
                NSArray *liItems = [obj findChildTags:@"li"];
                for (HTMLNode *node in liItems){
                    NSString *key = @"";
                    NSString *value = @"";
                    for (HTMLNode *spanNode in [node findChildTags:@"span"]){
                        if ([[spanNode getAttributeNamed:@"class"] isEqualToString:@"kng-width-33 kng-bold-text"]){
                            key = [spanNode contents];
                        }
                        
                        if ([[spanNode getAttributeNamed:@"class"] isEqualToString:@"kng-width-66"]){
                            value = [spanNode contents];
                        }
                    }
                    
                    if (key != nil && value != nil){
                        if ([key isEqualToString:@"Location:"]){
                            self.entryObject.location = [[value componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
                        } else if ([key isEqualToString:@"Street Address:"]){
                            self.entryObject.streetAddress = [[value componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
                        } else if ([key isEqualToString:@"Event Time(s):"]){
                            self.entryObject.eventTimes = [[value componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
                        } else if ([key isEqualToString:@"Event Start Date:"]){
                            self.entryObject.startDate = [[value componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
                        } else if ([key isEqualToString:@"Event End Date:"]){
                            self.entryObject.endDate = [[value componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
                        }
                    }
                }
            }
        }];
        
        if (![self.entryObject.streetAddress isEqualToString:@""] && ![self.entryObject.location isEqualToString:@""] && ![self.entryObject.startDate isEqualToString:@""] && ![self.entryObject.endDate isEqualToString:@""]){
            callback(self.entryObject);
         }
    }
}

@end
