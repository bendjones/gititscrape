//
//  GISScrapeControllerDelegate.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GISEntryObject;
@protocol GISScrapeControllerDelegate <NSObject>
@required
- (void)downloadsStarted;
- (void)downloadsFinished;
@optional
- (void)processedNodeCreated:(GISEntryObject *)newEntryObject;
- (void)logResult:(NSString *)logEntry;
- (void)downloadStatusChanged:(NSArray *)statusDescription;
@end
