//
//  GISEntryObject.m
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import "GISEntryObject.h"

#import <Firebase/Firebase.h>



@interface GISEntryObject ()
@property (strong, nonatomic) Firebase *firebase;
@end

@implementation GISEntryObject

- (id)init {
    if (self = [super init]){
        self.startDate = @"";
        self.endDate = @"";
        self.eventTimes = @"";
        self.location = @"";
        self.streetAddress = @"";
    }
    
    return self;
}

- (void)uploadToFirebaseWithDelegate:(id<GISEntryObjectDelegate>)delegate {
    self.firebase = [[Firebase alloc] initWithUrl:@"https://get-it-apricle.firebaseIO.com/"];
    [self.firebase authWithCredential:@"WM0Rjcz6h69Si3Kr4WJ1ibJT71KSaGKYlPG0BCYZ" withCompletionBlock:^(NSError *error, id data) {
        NSLog(@"Firebase Auth Successful");
    } withCancelBlock:^(NSError *error) {
        NSLog(@"Firebase Auth Failed with error: %@", [error localizedDescription]);
    }];
    
    [self.firebase observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
        if (delegate != nil && [delegate respondsToSelector:@selector(entryAddedToFirebase:)]){
            [delegate entryAddedToFirebase:self];
        }
    }];
    
    [[self.firebase childByAutoId] setValue:@{  @"location" : [self.location stringByTrimmingCharactersInSet:
                                                                            [NSCharacterSet whitespaceCharacterSet]],
                                                @"streetAddress" : [self.streetAddress stringByTrimmingCharactersInSet:
                                                                            [NSCharacterSet whitespaceCharacterSet]],
                                                @"eventTimes" : [self.eventTimes stringByTrimmingCharactersInSet:
                                                                            [NSCharacterSet whitespaceCharacterSet]],
                                                @"startDate" : [self.startDate stringByTrimmingCharactersInSet:
                                                                            [NSCharacterSet whitespaceCharacterSet]],
                                                @"endDate" : [self.endDate stringByTrimmingCharactersInSet:
                                                                            [NSCharacterSet whitespaceCharacterSet]]
                                            }];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Location: %@\nAddress: %@\nEvent Time: %@\nStart Date: %@\nEnd Date: %@",
            self.location, self.streetAddress,
            self.eventTimes, self.startDate, self.endDate];
}

@end
