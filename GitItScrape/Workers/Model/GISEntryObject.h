//
//  GISEntryObject.h
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GISEntryObjectDelegate.h"

@interface GISEntryObject : NSObject

@property (weak, nonatomic) id<GISEntryObjectDelegate>delegate;

@property (strong, nonatomic) NSString *location;
@property (strong, nonatomic) NSString *streetAddress;
@property (strong, nonatomic) NSString *eventTimes;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *endDate;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *basicInfo;
@property (strong, nonatomic) NSString *imageURL;

- (void)uploadToFirebaseWithDelegate:(id<GISEntryObjectDelegate>)delegate;

@end
