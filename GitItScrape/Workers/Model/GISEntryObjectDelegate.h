//
//  GISEntryObjectDelegate.h
//  GitItScrape
//
//  Created by Ben Jones on 5/27/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GISEntryObject;
@protocol GISEntryObjectDelegate <NSObject>
@required
- (void)entryAddedToFirebase:(GISEntryObject *)entryObject;
@end
