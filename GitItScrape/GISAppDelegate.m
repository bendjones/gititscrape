//
//  GISAppDelegate.m
//  GitItScrape
//
//  Created by Ben Jones on 5/24/13.
//  Copyright (c) 2013 Apricle Technologies. All rights reserved.
//

#import "GISAppDelegate.h"
#import "GISOverviewWindowController.h"

@implementation GISAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    self.overviewWindowController = [[GISOverviewWindowController alloc] init];
    [self.overviewWindowController.window makeKeyAndOrderFront:self];
}

@end
