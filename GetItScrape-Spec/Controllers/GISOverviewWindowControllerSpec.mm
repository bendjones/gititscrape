#import "GISOverviewWindowController.h"
#import <OCMock.h>

using namespace Cedar::Matchers;
using namespace Cedar::Doubles;

SPEC_BEGIN(GISOverviewWindowControllerSpec)

describe(@"GISOverviewWindowController", ^{
    __block GISOverviewWindowController *windowController;

    beforeEach(^{
        windowController = [[GISOverviewWindowController alloc] init];
    });
    
    it(@"should have a valid window", ^{
        windowController.window should_not be_nil;
    });
});

SPEC_END
